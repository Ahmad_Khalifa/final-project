module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      "color": {
        "gold": "rgb(255, 215, 0)",
      }, 
    },
  },
  plugins: [],
}
