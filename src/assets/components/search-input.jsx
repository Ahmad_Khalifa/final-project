import React, { useContext } from "react";
import { JobContext } from "../Contexts/data-job";

const SearchInput = () => {

    const {onChange, searchInput} = useContext(JobContext);

    return (
            <div>
                <form>
                    <input type="text" placeholder="Title, company or city" onChange={onChange} value={searchInput} className=" rounded-lg flex-1 appearance-none border border-gray-300 w-[300px] h-10 py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent mr-2 mb-[10px]" />
                </form>
            </div>
    )
        
};

export default SearchInput;