import React, { useContext } from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import { JobContext } from "../Contexts/data-job";

const Dashboard = () => {

    const {isAuth, history} = useContext(JobContext);
    
    if (!isAuth) {
        history.push("/login");
        return null;
    }

    return (
        <div className="bg-gray-100 flex justify-center min-h-[600px] md:min-h-[350px]">
            <div className="w-[400px] sd:w-[600px] md:w-[750px] bg-white my-[10px] rounded-xl p-[15px]">
                <h1 className="pl-[10px] text-[30px] text-gray-600 font-normal border-b-2 border-gray-200">Dashboard</h1>
                <div className="">
                    <div className="text-gray-600 text-[24px] bg-gray-100 mt-[10px] w-full p-[10px]">
                        <h2><i className="fas fa-paperclip"></i>  My jobs</h2>
                    </div>
                    <div className="flex p-[10px]">
                        <img src="/images/image-dashboard.jpg" className="w-[100px]"/>
                        <div className="w-full pt-[10px]">
                            <Link to="/dashboard/list-job-vacancy" className="md:ml-[70%] ml-[20px]"><button className="py-3 px-6 mr-6 bg-white hover:bg-gray-50 active:bg-gray-100 text-blue-500 font-bold border-2 border-solid border-blue-500 rounded-3xl text-[17px]"><i className="fas fa-pen"></i>  Job setting</button></Link>
                            <p className="border-t-2 mt-[10px] text-gray-600 pl-[20px]">-</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    )
};

export default Dashboard;
