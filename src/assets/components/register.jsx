import React, { useContext } from "react";
import { JobContext } from "../Contexts/data-job";


const Register = () => {
    const {onChangeInput, inputData, onRegister, textError1, textError2, isRegistered, isAuth, history} = useContext(JobContext);

    if (isAuth) {
        history.push("/");
        return null;
    }

    return (
            <div className="flex flex-col max-w-md px-4 py-8 bg-gray-100 rounded-lg shadow dark:bg-gray-800 sm:px-6 md:px-8 lg:px-10 mx-auto my-[20px] min-h-[71vh]">
                <div className="self-center mb-2 text-xl font-light text-gray-800 sm:text-2xl dark:text-white">
                Create a new account
                </div>
                <div className="p-6 mt-8">
                <form onSubmit={onRegister}>
                    {(isRegistered) && <p className="text-red-600 text-xs pl-3 my-1">Name or email has been registered</p>}
                    <div className="flex flex-col mb-2">
                    <div className=" relative ">
                        <input type="text" id="create-account-name" onChange={onChangeInput} value={inputData.name} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" name="name" placeholder="Name" required />
                    </div>
                    </div>
                    <div className="flex flex-col mb-2">
                    <div className=" relative ">
                        <input type="text" id="create-account-image-url" onChange={onChangeInput} value={inputData.image_url} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" name="image_url" placeholder="Your profile image (url)" required />
                    </div>
                    </div>
                    <div className="flex flex-col mb-2">
                    <div className=" relative ">
                        <input type="text" id="create-account-email" onChange={onChangeInput} value={inputData.email} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" name="email" placeholder="Email" required />
                    </div>
                    </div>
                    {(textError1) && <p className="text-red-600 text-xs pl-3 my-1">The password must have a minimum of 8 characters</p>}
                    <div className="flex flex-col mb-2">
                    <div className=" relative ">
                        <input type="password" id="create-account-password" onChange={onChangeInput} value={inputData.password} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" name="password" placeholder="Password" required />
                    </div>
                    </div>
                    {(textError2) && <p className="text-red-600 text-xs pl-3 my-1">Confirm password is not the same</p>}
                    <div className="flex flex-col mb-2">
                    <div className=" relative ">
                        <input type="password" id="create-account-confirm-password" onChange={onChangeInput} value={inputData.confirmPassword} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" name="confirmPassword" placeholder="Confirm password" required />
                    </div>
                    </div>
                    <div className="flex w-full my-4">
                    <button type="submit" className="py-2 px-4  bg-blue-500 hover:bg-blue-700 focus:ring-blue-500 focus:ring-offset-blue-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                        Register
                    </button>
                    </div>
                </form>
                </div>
            </div>
    )
};

export default Register;
