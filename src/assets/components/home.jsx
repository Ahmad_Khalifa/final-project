import React from "react";
import SearchInput from "./search-input";
import JobCart from "./job-cart";

const Home = () => {

    return (
        <div>
            <div className="flex justify-center w-full p-0 bg-white">
                <div className="w-[1000px] px-[10px] pt-10 flex flex-row flex-wrap-reverse md:flex-nowrap justify-center">
                    <div className="ml-[10px] mb-[20px]">
                        <h1 className="text-[48px] font-thin mb-[20px] text-gray-900">Welcome to your professional community</h1>
                        <SearchInput/>
                    </div>
                    <img src="/images/image-home.jpg" alt="image-home" className=" w-[450px]" />
                </div>
            </div>
            <JobCart/>
        </div>
    )
};

export default Home;