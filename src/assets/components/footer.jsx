import React from "react";

const Footer = () => {

    return (
            <footer className="bg-gray-800 w-full py-8 bottom-0 relative bottom-0">
                <div className="max-w-screen-xl px-4 mx-auto">
                    <ul className="flex flex-wrap justify-between max-w-screen-md mx-auto text-lg font-light">
                        <li className="my-2">
                            <a className="text-gray-300 hover:text-white transition-colors duration-200" href="#">
                                About
                            </a>
                        </li>
                        <li className="my-2">
                            <a className="text-gray-300 hover:text-white transition-colors duration-200" href="#">
                                Our Services
                            </a>
                        </li>
                        <li className="my-2">
                            <a className="text-gray-300 hover:text-white transition-colors duration-200" href="#">
                                Contact
                            </a>
                        </li>
                    </ul>
                </div>
            </footer>
    )
};

export default Footer;