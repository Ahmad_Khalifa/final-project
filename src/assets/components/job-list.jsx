import React, { useContext } from "react";
import { JobContext } from "../Contexts/data-job";
import SearchInput from "./search-input";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";

const JobList = () => {

    const {dataJob, rupiah, showJobList, setShowJobList, onClickButtonList, detailJobList, idDetailJob, searchInput, filterData, formStatus, onForm, onDelete, onEdit, showAlert, isAuth, history} = useContext(JobContext);

    if (!isAuth) {
        history.push("/login");
        return null;
    }

    let data;
    let {params} = useParams();

    if (searchInput != "") {
        data = filterData;
    } else {
        data = dataJob;
    }


    return  (
        <>
        {showAlert === "deleted" && (
            <div className="bg-red-200 border-red-600 text-red-600 border-l-4 p-4 w-72 fixed z-50 ml-3 mt-3" role="alert">
              <p className="font-bold">Success</p>
              <p>Data has been deleted.</p>
            </div>
          )}

            {showAlert === "created" && (
            <div className="bg-green-200 border-green-600 text-green-600 border-l-4 p-4 w-72 fixed z-50 ml-3 mt-3" role="alert">
              <p className="font-bold">Success</p>
              <p>Data has been created.</p>
            </div>
          )}  

            {showAlert === "edited" && (
            <div className="bg-orange-200 border-orange-600 text-orange-600 border-l-4 p-4 w-72 fixed z-50 ml-3 mt-3" role="alert">
              <p className="font-bold">Success</p>
              <p>Data has been edited.</p>
            </div>
          )}
        <div className="text-gray-700 bg-gray-100 min-h-[600px] md:min-h-[350px] flex flex-col items-center pb-[10px]">  
            <button onClick={onForm} className="py-3 px-6 mr-6 bg-white hover:bg-gray-50 active:bg-gray-100 text-blue-500 font-bold border-2 border-solid border-blue-500 rounded-3xl text-[17px] h-fit w-fit my-[10px]"><i className="fas fa-edit"></i>  Post a job</button>
            <SearchInput/>
            {data.map(value => {
                    
                    const buttonDetail = () => {
                        if (showJobList && idDetailJob == value.id) {
                            setShowJobList(false);
                        } else {
                            onClickButtonList();
                        }
                    }
                    
                    let createdDate = new Date(value.createdAt);
                    let currentDate = new Date();
                    const timeDifference = currentDate - createdDate;   
                    const daysDifference = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
                    let jobStatus;
                    let timeCreate;

                    if (value.jobStatus == 1) {
                        jobStatus = "Active";
                    } else {
                        jobStatus = "Closed"
                    }

                    if (daysDifference == 0) {
                        timeCreate = "Today"
                    } else {
                        timeCreate = `${daysDifference} days ago`
                    }

                    return (
                        <div key={value.id + 222} className="w-[400px] sm:w-[550px] md:w-[700px] relative bg-white rounded-md overflow-auto p-[10px] mb-[10px] text-gray-700">
                            <div className="flex gap-2">
                                <img className="w-0 sm:w-[80px] text-gray-700 text-[12px]" src={value.companyImage} alt="Company image" />
                                <div className="absolute right-[10px] top-[10px] font-bold text-[20px]  text-gray-500 flex gap-2">
                                    <button type="button" onClick={onEdit} value={value.id} className="py-2 px-2  bg-green-600 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg fas fa-pencil-alt ">
                                    </button>
                                    <button type="button" onClick={onDelete} value={value.id} className="py-2 px-2  bg-red-600 hover:bg-red-700 focus:ring-red-500 focus:ring-offset-red-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg fas fa-trash">
                                    </button>
                                </div>
                                <div className=" text-gray-700">
                                    <h1 className="text-[24px] w-[300px] sm:w-[350px] text-left">{value.title} ({value.jobTenure})</h1>
                                    <p className="text-[15px]">{value.companyName}, {value.companyCity} - <span className="text-gray-500">{jobStatus}, {timeCreate}</span></p>
                                    <button onClick={buttonDetail} value={value.id} className="text-[14px] px-[2px] py-[1px] border-[1px] rounded-[5px] hover:bg-gray-100 active:bg-gray-200">Detail job</button>
                                </div>
                                
                            </div>
                            <div>
                                {(showJobList && idDetailJob == value.id) &&  
                                <>
                                    <div className="text-gray-800 my-[20px] ml-[20px]">
                                        <p><i className="fas fa-briefcase"></i><span className="m-[3px]"></span> {detailJobList.jobType}</p>
                                        <div className="flex flex-row text-gray-800"><i className="fas fa-money-bill-wave"></i><p className="ml-[10px]">{rupiah(detailJobList.salaryMin)} - {rupiah(detailJobList.salaryMax)}</p></div>
                                    </div>
                                    <p className="text-[15px] text-gray-700 mt-[15px] mb-[10px] text-justify p-[3px]"><span className="font-medium text-gray-700 text-[17px]">Requirements</span> <br/>{detailJobList.jobQualification}</p>
                                    <p className="text-[15px] text-gray-700 mt-[15px] mb-[10px] text-justify p-[3px]"><span className="font-medium text-gray-700 text-[17px]">About the job</span> <br/> {detailJobList.jobDescription}</p>   
                                </>
                             }
                                </div>

                            
                        </div>
                    )
            })}
        </div>
        </>
        )
    
};

export default JobList;
