import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import { JobContext } from "../Contexts/data-job";


const NavBar = () => {

    const {isAuth, onLogout, show, setShow, setInputData } = useContext(JobContext);

    
    return (
        <nav className="flex flex-row w-full h-20 bg-white text-gray-500 justify-between items-center " >
            <img className="h-20 ml-[5px]" src="/images/JC-Logo.jpg" alt="Logo" />  
            <ul>
                <li className="opacity-90 hover:text-gray-900 font-bold">
                    <Link to="/">Home</Link>  
                </li>
            </ul>
            <div>
                {!isAuth && <Link to="/register"><button className="py-3 px-6 mr-2 text-gray-500 font-bold hover:bg-gray-100 active:bg-gray-200 rounded-3xl">Join now</button></Link>}
                {!isAuth && <Link to="/login"><button className="py-3 px-6 mr-6 bg-white hover:bg-gray-50 active:bg-gray-100 text-blue-500 font-bold border-2 border-solid border-blue-500 rounded-3xl">Sign in</button></Link>}
                {isAuth && 
                    <>    
                    <button onClick={() => {setShow((!show) ? true : false)}} className="text-[30px] text-gray-400 font-bold hover:text-gray-600 active:text-gray-900 p-0 mr-[20px]"><i className="fas fa-user-circle"></i></button>  
                    {(show) && <div className="absolute right-2 rounded-[10px] shadow-md border-[1px] border-solid border-gray-300 p-[10px] bg-white z-50">
                        <ul className="text-center">
                            <li>
                            <Link to="#">   
                                <button className="py-1 px-6 bg-white hover:bg-gray-50 active:bg-gray-100 text-blue-500 font-bold border-2 border-solid border-blue-500 rounded-3xl text-[15px] w-fit">View Profil</button>
                            </Link>
                            </li>
                            <li>
                            <Link to="/dashboard">
                                <button onClick={()=>{setShow(false)}} className=" text-gray-500 hover:text-gray-900 font-medium">Dashboard</button>
                            </Link>
                            </li>
                            <li>
                            <Link to="/change-password">
                                <button onClick={()=>{setShow(false); {setInputData("")}}} className=" text-gray-500 hover:text-gray-900 font-medium">Change Password</button>
                            </Link>
                            </li>
                            <li>
                            <Link to="/">
                                <button onClick={onLogout} className=" text-gray-500 hover:text-gray-900 font-medium">Sign out</button>
                            </Link>
                            </li>
                        </ul>
                    </div>}
                    </>
            }
            </div>
        </nav>
    )
};

export default NavBar;