import React, { useContext } from "react";
import { JobContext } from "../Contexts/data-job";

const ChangePasswordForm = () => {

    const {textError1,textError2, textError3, onChangeInput, inputData, onSubmitChangePassword, history, isAuth} = useContext(JobContext);

    if (!isAuth) {
        history.push("/login");
        return null;
    }

    return (
            <div className="flex flex-col min-h-[600px] md:min-h-[350px] max-w-md px-4 py-8 bg-white rounded-lg shadow dark:bg-gray-800 sm:px-6 md:px-8 lg:px-10 mx-auto my-[20px]">
                <div className="self-center mb-2 text-xl font-light text-gray-800 sm:text-2xl dark:text-white">
                Change Your Password
                </div>
                <div className="p-6 mt-8">
                <form onSubmit={onSubmitChangePassword}>
                {(textError3) && <p className="text-red-600 text-xs pl-3 my-1">The current password is incorrect</p>}
                    <div className="flex flex-col mb-2">
                    <div className=" relative ">
                        <input type="password" id="current-password" onChange={onChangeInput} value={inputData.password} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" name="current_password" placeholder="Current password" required />
                    </div>
                    </div>
                    {(textError1) && <p className="text-red-600 text-xs pl-3 my-1">The password must have a minimum of 8 characters</p>}
                    <div className="flex flex-col mb-2">
                    <div className=" relative ">
                        <input type="password" id="new-password" onChange={onChangeInput} value={inputData.password} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" name="new_password" placeholder="New password" required />
                    </div>
                    </div>
                    {(textError2) && <p className="text-red-600 text-xs pl-3 my-1">Confirm password is not the same</p>}
                    <div className="flex flex-col mb-2">
                    <div className=" relative ">
                        <input type="password" id="new-confirm-password" onChange={onChangeInput} value={inputData.confirmPassword} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" name="new_confirm_password" placeholder="Confirm new password" required />
                    </div>
                    </div>
                    <div className="flex w-full my-4">
                    <button type="submit" className="py-2 px-4  bg-blue-500 hover:bg-blue-700 focus:ring-blue-500 focus:ring-offset-blue-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                        Submit
                    </button>
                    </div>
                </form>
                </div>
            </div>
    )
};

export default ChangePasswordForm;
