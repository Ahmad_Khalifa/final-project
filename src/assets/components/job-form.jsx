import React, { useContext } from "react";
import { JobContext } from "../Contexts/data-job";

const JobForm = () => {

    const { onChangeInput, inputData, onSubmitForm, isAuth, history } = useContext(JobContext);

    if (!isAuth) {
        history.push("/login");
        return null;
    }

    return (
        <div className="bg-gray-100 w-full flex justify-center">
            <div className="w-[400px] sm:w-[550px] md:w-[700px] relative bg-white rounded-md flex flex-col items-center my-[15px]">
                <h1 className="text-[30px] text-gray-700 font-medium">Job Form</h1>
                <form onSubmit={onSubmitForm}>    
                    <div className=" relative w-[375px] sm:w-[400px] mt-[15px] mb-[10px]">
                        <label htmlFor="title" className="text-gray-700">
                            Title
                            <span className="text-red-500 required-dot">
                                *
                            </span>
                        </label>
                        <input type="text" id="title" onChange={onChangeInput} value={inputData.title} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-400 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" name="title" placeholder="Title..." required/>
                    </div>
                    <div className=" relative w-[375px] sm:w-[400px] mt-[15px] mb-[10px]">
                        <label htmlFor="company-name" className="text-gray-700">
                            Company name
                            <span className="text-red-500 required-dot">
                                *
                            </span>
                        </label>
                        <input type="text" id="company-name" onChange={onChangeInput} value={inputData.company_name} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-400 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" name="company_name" placeholder="Company name..." required/>
                    </div>
                    <div className=" relative w-[375px] sm:w-[400px] mt-[15px] mb-[10px]">
                        <label htmlFor="company-city" className="text-gray-700">
                            Company city
                            <span className="text-red-500 required-dot">
                                *
                            </span>
                        </label>
                        <input type="text" id="company-city" onChange={onChangeInput} value={inputData.company_city} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-400 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" name="company_city" placeholder="Company city..." required/>
                    </div>
                    <div className=" relative w-[375px] sm:w-[400px] mt-[15px] mb-[10px]">
                        <label htmlFor="job-tenure" className="text-gray-700">
                            Job tenure
                            <span className="text-red-500 required-dot">
                                *
                            </span>
                        </label>
                        <input type="text" id="job-tenure" onChange={onChangeInput} value={inputData.job_tenure} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-400 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" name="job_tenure" placeholder="Contract, internship, etc..." required/>
                    </div>
                    <div className=" relative w-[375px] sm:w-[400px] mt-[15px] mb-[10px]">
                        <label htmlFor="job-type" className="text-gray-700">
                            Job type
                            <span className="text-red-500 required-dot">
                                *
                            </span>
                        </label>
                        <input type="text" id="job-type"  onChange={onChangeInput} value={inputData.job_type} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-400 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" name="job_type" placeholder="WFH, onsite, etc..." required/>
                    </div>
                    <div className=" relative w-[375px] sm:w-[400px] mt-[15px] mb-[10px]">
                        <label className="text-gray-700">
                            Salary
                            <span className="text-red-500 required-dot">
                                *
                            </span>
                        </label>
                        <div className="flex items-center">
                            <input type="number" id="salary-min" value={inputData.salary_min} onChange={onChangeInput} className="inline-block rounded-lg border-transparent flex-1 appearance-none border border-gray-400 w-[150px] py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" name="salary_min" placeholder="Minimal" required/>
                            <p className="mx-[5px] text-[30px] font-medium text-gray-700">-</p>
                            <input type="number" id="salary-max" onChange={onChangeInput} value={inputData.salary_max} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-400 w-[150px] py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" name="salary_max" placeholder="Maximal" required/>
                        </div>
                    </div>
                    <div className=" relative w-[375px] sm:w-[400px] mt-[15px] mb-[10px]">
                        <label htmlFor="job-qualification" className="text-gray-700">
                            Requirement/qualification
                            <span className="text-red-500 required-dot">
                                *
                            </span>
                        </label>
                        <textarea type="text" id="job-qualification" onChange={onChangeInput} value={inputData.job_qualification} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-400 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" name="job_qualification" placeholder="Qualification..." required/>
                    </div>
                    <div className=" relative w-[375px] sm:w-[400px] mt-[15px] mb-[10px]">
                        <label htmlFor="job-description" className="text-gray-700">
                            Description
                            <span className="text-red-500 required-dot">
                                *
                            </span>
                        </label>
                        <textarea type="text" id="job-description" onChange={onChangeInput} value={inputData.job_description} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-400 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" name="job_description" placeholder="Description..." required/>
                    </div>
                    <div className=" relative w-[375px] sm:w-[400px] mt-[15px] mb-[10px]">
                        <label htmlFor="company-image" className="text-gray-700">
                            Company image
                            <span className="text-red-500 required-dot">
                                *
                            </span>
                        </label>
                        <input type="text" id="company-image" onChange={onChangeInput} value={inputData.company_image_url} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-400 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" name="company_image_url" placeholder="Image (url)" required/>
                    </div>
                    <div className=" relative w-[375px] sm:w-[400px] mt-[15px] mb-[10px]">
                        <label htmlFor="company-image" className="text-gray-700">
                            Status
                            <span className="text-red-500 required-dot">
                                *
                            </span>
                        </label>
                        <br/>
                        <input type="radio" id="job-status-active" onChange={onChangeInput} value={1} className="text-gray-700" name="job_status" placeholder="Image (url)" required/>
                        <label className="text-gray-700 mr-[20px]" htmlFor="job-status-active"> Active</label>
                        <input type="radio" id="job-status-closed" onChange={onChangeInput} value={0} className="text-gray-700" name="job_status" placeholder="Image (url)" required/>
                        <label className="text-gray-700" htmlFor="job-status-closed"> Closed</label>
                    </div>
                    <button type="submit" className="py-2 px-4  bg-blue-500 hover:bg-blue-700 focus:ring-blue-500 focus:ring-offset-blue-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg mb-[20px] ">
                            Save
                    </button>
                </form>
            </div>
        </div>
    )
};

export default JobForm;
