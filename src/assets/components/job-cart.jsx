import React, { useContext, useState } from "react";
import { JobContext } from "../Contexts/data-job";

const JobCart = () => {

    const {dataJob, showDetail, detailJob, onClickButton, setShowDetail, setDetailJob, rupiah, filterData, searchInput} = useContext(JobContext);

    let data;
    const formatDate = (value) => {
        let createdDate = new Date(value);
        let currentDate = new Date();
        const timeDifference = currentDate - createdDate;   
        const daysDifference = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
        let timeCreate;

        if (daysDifference == 0) {
            timeCreate = "Today"
        } else {
            timeCreate = `${daysDifference} days ago`
        }
        return timeCreate;
    }

    if (searchInput != "") {
        data = filterData;
    } else {
        data = dataJob;
    }


    return (
        <div className="w-full bg-gray-200 flex flex-row justify-center min-h-[25vh]">
            <div className="w-[200px] sm:w-[400px] md:w-[450px] bg-white my-[17px] rounded-xl max-h-[600px] overflow-auto">
                <h2 className="text-black font-medium text-[20px] m-[10px]">More jobs for you</h2>
                    {data.map(value => {
                       
                        let jobStatus;

                        if (value.jobStatus == 1) {
                            jobStatus = "Active";
                        } else {
                            jobStatus = "Closed"
                        }

                        return (
                        <div key={value.id + 111} className="sm:flex m-[10px] pb-[5px] border-b-2">
                            <img src={value.companyImage} alt="Image" className="text-black w-[0px] sm:w-[50px] h-fit my-0 mx-auto sm:m-0"/>
                            <div className="sm:ml-[10px]">
                                <button onClick={onClickButton} value={value.id} className="text-indigo-500 hover:text-indigo-800 font-medium p-0 m-0 text-left">{value.title} ({value.jobTenure}) </button>
                                <p className="text-black text-[15px]">{value.companyName}</p>
                                <p className="text-gray-600 text-[15px]">{value.companyCity}</p>
                                <p className="text-gray-500 text-[13px]">{jobStatus}, {formatDate(value.createdAt)}</p>
                            </div>
                        </div>
                        )   
                    })}
            </div>
            {showDetail && 
                    <div className="sm:w-[450px] my-[17px] ml-[5px] p-[10px] bg-white text-black relative transition-all w-[300px] max-h-[600px] overflow-auto">
                        <button onClick={() => {setShowDetail(false); setDetailJob("");}} className="absolute right-[10px] top-[10px] font-bold text-[20px] text-gray-500 rounded-full hover:bg-gray-200 active:bg-gray-300 px-[8px]">X</button>
                        <h1 className="text-[25px] w-[200px] md:w-[350px]">{detailJob.title} ({detailJob.jobTenure})</h1>
                        <p className="text-[15px]">{detailJob.companyName}, {detailJob.companyCity} - <span className="text-gray-600">{formatDate(detailJob.createdAt)}</span></p>
                        <div className="text-gray-800 my-[20px] ml-[20px]">
                            <p><i className="fas fa-briefcase"></i><span className="m-[3px]"></span> {detailJob.jobType}</p>
                            <div className="flex flex-row text-gray-800"><i className="fas fa-money-bill-wave"></i><p className="ml-[10px]">{rupiah(detailJob.salaryMin)} - {rupiah(detailJob.salaryMax)}</p></div>
                        </div>
                        <p className="text-[15px] mt-[15px] mb-[10px] text-justify p-[3px]"><span className="font-medium text-gray-700 text-[17px]">Requirements</span> <br/>{detailJob.jobQualification}</p>
                        <p className="text-[15px] mt-[15px] mb-[10px] text-justify p-[3px]"><span className="font-medium text-gray-700 text-[17px]">About the job</span> <br/> {detailJob.jobDescription}</p>
                    </div>}
        </div>
    )
};

export default JobCart;