import { data } from "autoprefixer";
import axios from "axios";
import Cookies from "js-cookie";
import React, { createContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

export const JobContext = createContext();

const JobProvider = props => {

    const [dataJob, setDataJob] = useState([]);
    const [fetchStatus, setFetchStatus] = useState(true);
    const [currentId, setCurrentId] =  useState(null);
    const [searchStatus, setSearchStatus] = useState(false);
    const [searchInput, setSearchInput] = useState("");
    const [inputData, setInputData] = useState("");
    const [textError1, setTextError1] = useState(false);
    const [textError2, setTextError2] = useState(false);
    const [textError3, setTextError3] = useState(false);
    const [isRegistered, setIsRegistered] = useState(false);
    const [isAuth, setIsAuth] = useState((Cookies.get("token")) ? true : false);
    const [showDetail, setShowDetail] = useState(false);
    const [detailJob, setDetailJob] = useState("");
    const [detailJobList, setDetailJobList] = useState("");
    const [showJobList, setShowJobList] = useState(false);
    const [idDetailJob, setIdDetailJob] = useState(null);
    const [filterData, setFilterData] = useState([]);
    const [formStatus, setFormStatus] = useState("");
    const [show, setShow] = useState(false);
    const [user, setUser] = useState((Cookies.get("token")) ? {token: Cookies.get("token")} : "");
    const [showAlert, setShowAlert] = useState("");

    let history = useHistory();

    useEffect(() => {

        const fetchData = async () => {
            const result = await axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy`);
            
            const data = result.data.data.map(value => {
                return {
                    title: value.title,
                    companyCity: value.company_city,
                    companyName: value.company_name,
                    createdAt: value.created_at,
                    companyImage: value.company_image_url,
                    jobTenure: value.job_tenure,
                    jobType: value.job_type,
                    jobQualification: value.job_qualification,
                    jobDescription: value.job_description,
                    salaryMin: value.salary_min,
                    salaryMax: value.salary_max,
                    id: value.id,
                    jobStatus: value.job_status
                }
            });
            // console.log(result.data.data);
            setDataJob(data);
        }

        if (fetchStatus) {
            fetchData();
            setFetchStatus(false);
        }
    });

    const rupiah = (number)=>{
        return new Intl.NumberFormat("id-ID", {
          style: "currency",
          currency: "IDR"
        }).format(number);
      }

    const onClickButton = () => {
        let checkId = event.target.value;
        setIdDetailJob(checkId);
        setDetailJob(dataJob.find(x => { return x.id == checkId; }));

        if (!showDetail) {
            setShowDetail(true);        
        }

        
    }

    const onClickButtonList = () => {
        let checkId = event.target.value;
        setIdDetailJob(checkId);
        setDetailJobList(dataJob.find(x => { return x.id == checkId; }));

        if (!showJobList) {
            setShowJobList(true);        
        }

        
    }

    const onChange = () => {
        setSearchInput(event.target.value);
        const filter = dataJob.filter(value => value.title.toLowerCase().includes(searchInput.toLowerCase()) || value.companyCity.toLowerCase().includes(searchInput.toLowerCase()) || value.companyName.toLowerCase().includes(searchInput.toLowerCase()) || value.jobTenure.toLowerCase().includes(searchInput.toLowerCase()) );

        setFilterData(filter);
    };

    const onChangeInput = () => {
        const {name, value} = event.target;
        setInputData({...inputData, [name]: value});
    };

    const onRegister = (event) => {
        event.preventDefault();
        if (inputData.password.length < 8) {
            setTextError1(true);
        }
        if (inputData.password != inputData.confirmPassword) {
            setTextError2(true);
        }

        if ( inputData.password.length >= 8 && inputData.password == inputData.confirmPassword ) {
            axios.post(`https://dev-example.sanbercloud.com/api/register`, {name: inputData.name, image_url: inputData.image_url, email: inputData.email, password: inputData.password})
            .then(response => {
                console.log(response.data.data);
                alert("Registration successful!");
                setInputData("");
                setIsRegistered(false);
                setTextError1(false);
                setTextError2(false);
            })
            .catch(error => {
                console.error("Registration error:", error.response ? error.response.data : error.message);
                alert("Registration failed. Please check your information and try again.");
                setIsRegistered(true);
            })
        }
    };

    const onLogin = (event) => {
        event.preventDefault();
        axios.post(`https://dev-example.sanbercloud.com/api/login`, {...inputData})
        .then(res => {
            let token = res.data.token;
            Cookies.set("token", token);
            setIsAuth(true);
            setInputData("");
            setShowDetail(false);
            alert("Berhasil login");
            history.push("/");
        })
        .catch(() => {
            alert("Email atau password yang anda masukkan salah")
        })
    };

    const onLogout = () => {
        Cookies.remove("token");
        setIsAuth(false);
        setShowDetail(false);
        setShow(false);
        setUser("");
        history.push("/");
    }

    const onForm = () => {
        history.push("/dashboard/list-job-vacancy/create");
        setUser({token: Cookies.get("token")});
        setCurrentId(null);
        setInputData("");
        setFormStatus("create");
    };

    const onSubmitForm = (event) => {
        event.preventDefault();

        if (currentId !== null) {
            axios.put(`https://dev-example.sanbercloud.com/api/job-vacancy/${currentId}`, {...inputData}, {headers: {"Authorization" : "Bearer "+ user.token}})
            .then(()=> {
                setCurrentId(null);
                setInputData("");
                history.push("/dashboard/list-job-vacancy");
                setFetchStatus(true);
                setShowAlert("edited");
                setTimeout(() => {
                    setShowAlert("");
                }, 4000)
            })
        } else {
            axios.post(`https://dev-example.sanbercloud.com/api/job-vacancy`, {...inputData}, {headers: {"Authorization" : "Bearer "+ user.token}})
            .then(() => {
                setInputData("");
                history.push("/dashboard/list-job-vacancy");
                setFetchStatus(true);
                setShowAlert("created");
                setTimeout(() => {
                    setShowAlert("");
                }, 4000)
            })
            .catch((error) => {
                (error.response) ? console.log(error.response.data) : error.message;
            })
        }
        
    };

    const onDelete = () => {
        let checkId = event.target.value;
        
        axios.delete(`https://dev-example.sanbercloud.com/api/job-vacancy/${checkId}`, {headers: {"Authorization" : "Bearer "+ user.token}})
        .then(()=> {
            setFetchStatus(true);
            setShowAlert("deleted");
            setTimeout(() => {
                setShowAlert("");
            }, 4000)
        })
        
    }

    const onEdit = () => {
        let checkId = event.target.value;
        setCurrentId(checkId);
        let dataEdit = dataJob.find(x => x.id == checkId);
        setInputData({
            title: dataEdit.title,
            job_tenure: dataEdit.jobTenure,
            job_type: dataEdit.jobType,
            salary_min: dataEdit.salaryMin,
            salary_max: dataEdit.salaryMax,
            job_description: dataEdit.jobDescription,
            job_qualification: dataEdit.jobQualification,
            job_status: dataEdit.jobStatus,
            company_city: dataEdit.companyCity,
            company_name: dataEdit.companyName,
            company_image_url: dataEdit.companyImage
        });
        history.push("/dashboard/list-job-vacancy/edit");
    }

    const onSubmitChangePassword = (event) => {
        event.preventDefault();

        if (inputData.new_password.length < 8) {
            setTextError1(true);
        }
        if (inputData.new_password != inputData.new_confirm_password) {
            setTextError2(true);
        }

        if ( inputData.new_password.length >= 8 && inputData.new_password == inputData.new_confirm_password ) {
            axios.post(`https://dev-example.sanbercloud.com/api/change-password`, {...inputData}, {headers : {"Authorization" : "Bearer "+ user.token}})
            .then(() => {
                alert("Change password success");
                history.push("/");
                setTextError1(false);
                setTextError2(false);
                setTextError3(false);
            }).catch((error)=> {
                error.response ? console.log(error.response.data) : console.log(error.message);
                setTextError3(true);
            })
        }
    }

    
    return (
        <JobContext.Provider value={{
            dataJob, setDataJob, searchInput, setSearchInput, onChange, searchStatus, setSearchStatus, setFetchStatus, onChangeInput, inputData, onRegister, textError1, textError2, textError3, isRegistered, onLogin, isAuth, onLogout, showDetail, onClickButton, detailJob, setShowDetail, setDetailJob, history, showJobList, setShowJobList, rupiah, idDetailJob, filterData, onForm, formStatus, onSubmitForm, show, setShow, onDelete, onClickButtonList, detailJobList, onEdit, onSubmitChangePassword, setInputData, showAlert
        }}>
            {props.children}
        </JobContext.Provider>
    );
};

export default JobProvider;


