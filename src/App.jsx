import React from "react";
import NavBar from "./assets/components/nav-bar";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom/cjs/react-router-dom.min";
import Register from "./assets/components/register";
import Login from "./assets/components/login";
import Home from "./assets/components/home";
import Footer from "./assets/components/footer";
import JobProvider from "./assets/Contexts/data-job.jsx"
import Dashboard from "./assets/components/dashboard.jsx";
import JobList from "./assets/components/job-list.jsx";
import JobForm from "./assets/components/job-form.jsx";
import ChangePasswordForm from "./assets/components/change-password-form.jsx";


const App = () => {
  

  
  return (
    <Router>
      <JobProvider>
        <div className="min-h-screen">
          <NavBar/>
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/register" component={Register} exact />
            <Route path="/login" component={Login} exact />
            <Route path="/dashboard" component={Dashboard} exact />
            <Route path="/dashboard/list-job-vacancy" component={JobList} exact />
            <Route path="/dashboard/list-job-vacancy/:params" component={JobForm} exact />
            <Route path="/change-password" component={ChangePasswordForm} exact />
          </Switch>
          <Footer/>
        </div>
      </JobProvider>
    </Router>
  )
};

export default App;
